#include <stdio.h>

int inputfunction()
{
    int a;
    printf("Enter a prime or composite number. Don't enter 1\n");
    scanf("%d", &a);
    return a;
}
int computefunction(int a)
{
    int x, i=2;
    if (a%2 == 0)
        x = a/2;
    else
        x = (a+1)/2;
    while(i<=x)
    {
        if (a%i == 0)
            return 0;
        else 
            return 1;
		i += 1;
    }
}
void outputfunction(int a,int f)
{
    int y;
    y = computefunction(a);
    if (y == 0)
        printf("The number is composite\n");
    else if (y == 1)
        printf("The number is prime\n");
}
int main()
{
    int a, f;
    a = inputfunction();
    f = computefunction(a);
    outputfunction(a,f);
    return 0;
}