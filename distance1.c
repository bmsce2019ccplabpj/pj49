#include <stdio.h>
#include <math.h>
int main()
{
    float x1,y1,x2,y2;
    float dist;
    printf("Enter the coordinates of first pt\n");
    scanf("%f %f", &x1,&y1);
    printf("Enter the coordinates of second pt\n");
    scanf("%f %f", &x2,&y2);
    dist = sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
    printf("The distance is %f\n", dist);
    return 0;
}