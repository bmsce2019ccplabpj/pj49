#include<stdio.h>
int main()
{
    int n,e;
    printf("Input the array length");
    scanf("%d",&n);
    int a[n];
    printf("Enter a sorted array");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("Enter the value to be searched:\n");
    scanf("%d",&e);
    int beg=0,mid,end=n-1,flag;
    while(beg<=end)
    {
        mid=(beg+end)/2;
        if(a[mid]==e)
        {
            flag=1;
        }
        else if(a[mid]<e)
        { 
            mid=beg+1;
        }
        else
        {
            mid=end-1;
        }
    }    
    if(flag==1)
        printf("The search element exists in the array and its position is %d",mid);
    else
        printf("Search element does not exist in the array\n");
    return 0;
}