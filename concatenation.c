#include <stdio.h>

void input_function(char str1[],char str2[])
{
    printf("Enter two strings\n");
    gets(str1);
    gets(str2);
}

int length_function(char str[])
{
    int i=0;
    while(str[i]!='\0')
    i++;
    return i;
}

void compute_function(char str1[],char str2[])
{
    int l1,l2,i;
    l1 = length_function(str1);
    l2 = length_function(str2);
    for(i=0;i<l2;i++)
    {
        str1[l1+i] = str2[i];
    }
}

void output_function(char str[])
{
    int i=0;
    printf("The concatenated string is ");
    while(str[i]!='\0')
    {
        printf("%c",str[i]);
        i++;
    }
}

int main()
{
    char str1[100],str2[100];
    input_function(str1,str2);
    compute_function(str1,str2);
    output_function(str1);
    return 0;
}