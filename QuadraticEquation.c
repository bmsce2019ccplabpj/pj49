#include<stdio.h>
#include<math.h>

void input(float *x,float *y,float *z)
{
    printf("Enter x,y and z");
    scanf("%f%f%f",x,y,z);
}

void roots(float x,float y,float z,int *i,float *p,float *j, float d)
{
    float d = (y*y)-(4*x*z);
    if(d>0)
    {
        *i = 2;
        *p = (-y + sqrt(d))/(2*x);
        *j = (-y - sqrt(d))/(2*x);
    }
    else if(d==0)
    {
        *i = 1;
        *p = (-y)/(2*x);
    }
    else
    {
    *i = 0;
    }
}

void output(int i,float p,float j)
{
    if(i==2)
    printf("There are two real roots :\nx1 = %f\nx2 = %f\n",p,j);
    else if(i==1)
    printf("Both roots are equal\nx = %f\n",p);
    else
    printf("No real roots\n");
}

int main()
{
    float x,y,z,p,j;
    int i;
    input(&x,&y,&z);
    roots(x,y,z,&i,&p,&j);
    output(i,p,j);
    return 0;
}