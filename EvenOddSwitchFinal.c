#include<stdio.h>
int input()
{
    int n;
    printf("Enter the number to be checked\n");
    scanf("%d",&n);
    return n;
}
int check(int n)
{
    switch (n%2)
    {
        case (0): return 0;
                  break;
        case (1): return 1;
                  break;
    }
}
void output(int c)
{
    if(c==0)
        printf("Entered number is even\n");
    else
        printf("Entered number is odd\n");
}
int main()
{
    int n,c;
    n = input();
    c = check(n);
    output(c);
    return 0;
}