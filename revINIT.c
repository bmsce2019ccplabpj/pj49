#include <stdio.h>

int inputfunction()
{
    int a;
    printf("Enter the number to be reversed\n");
    scanf("%d",&a);
    return a;
}

int computefunction(a)
{
    int d,rev=0;
    while(a>0)
    {
        d=a%10;
        rev=rev*10+d;
        a=a/10;
    }
    return rev;
}
void outputfunction(a, rev)
{
   printf("Reverse of the number is %d\n",rev);
   if(a==rev)
       printf("%d is a reversible no\n",a);
   else
       printf("%d is not a reversible no\n",a);
}
int main()
{
   int a,rev;
   a=inputfunction();
   rev=computefunction(a);
   outputfunction(a,rev);
   return 0;
}