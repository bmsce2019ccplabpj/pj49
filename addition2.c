##include <stdio.h>
float input_function(x)
{
    float i;
    if(x==1)
    {
        printf("Enter the %dst number\n", x);
        scanf("%f", &i);
    }
    else
	{
        printf("Enter the %dnd number\n", x);
        scanf("%f", &i);
	}
    return i;
}
float compute_function(float a,float b)
{
    float sum;
    sum = a + b;
    return sum;
}
void output_function(float a,float b,float sum)
{
    printf("The sum of %f and %f is %f", a,b,sum);
}
int main()
{
    float a,b,sum;
    a = input_function(1);
    b = input_function(2);
    sum = compute_function(a,b);
    output_function(a,b,sum);
    return 0;
}
    