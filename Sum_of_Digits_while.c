#include <stdio.h>

int input()
{
    int n;
    printf("Enter the number\n");
    scanf("%d",&n);
    return n;
}

int compute(int n)
{
    int x,a=0;
    while(n>0)
    {
        x=n%10;
        a=a+x;
        n=n/10;
    }
    return a;
}

void output(int a)
{
    printf("Sum is = %d\n",a);
}

int main()
{
    int n;
    n=input();
    n=compute(n);
    output(n);
    return 0;
}