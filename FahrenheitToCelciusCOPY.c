#include <stdio.h>
void convert(float);
void output(float);

void input()
{
	float fht;
	printf("Enter the temperature in Degree Fahrenheit\n");
	scanf("%f",&fht);
	convert(fht);
}
convert(float fht)
{
	float cel =(5.0/9.0)*(fht-32);
    output(cel);
}
output(float cel)
{
    printf("Temperature in Degree Celsius = %.2f\n",cel);
}
int main()
{
    input();
    return 0;
}