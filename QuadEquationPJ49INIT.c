#include <stdio.h>
#include<math.h>

int main()
{
    float a,b,c,d;
    float r1,r2;
    printf("Enter a,b,c of the quad equation\n");
    scanf("%d%d%d",&a,&b,&c);
    d = sqrt(b*b-4*a*c);
    r1 = (b+d)/2*a;
    r2 = (b-d)/2*a;
    printf("The roots are %f and %f", r1,r2);
    return 0;
}